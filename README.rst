READ ME
=======

This directory contains the source code for the Bitbucket Shell.

Introduction to the Bitbucket Shell
-----------------------------------

The Bitbucket Shell is an interactive command-line interface to `Bitbucket Cloud`_.

This program is free software: you can redistribute it and/or modify it
under the terms of the `GNU General Public License`_ as published
by the `Free Software Foundation`_, either version 3 of the License,
or (at your option) any later version.

.. _Bitbucket Cloud: https://bitbucket.org/
.. _GNU General Public License: https://www.gnu.org/licenses/gpl.html
.. _Free Software Foundation: https://www.fsf.org/
