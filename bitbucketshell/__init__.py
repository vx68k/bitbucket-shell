# bitbucketshell/__init__.py - primary module of bitbucket-shell
# Copyright (C) 2017 Kaz Nishimura
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
# the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""interactive command-line interface to Bitbucket Cloud
"""

from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
from cmd import Cmd
import sys

class BitbucketShell(object):
    """
    Bitbucket Shell.
    """

    def __init__(self):
        self.cmd = BitbucketCmd()

    def run(self):
        """
        Run this shell.
        """
        self.cmd.cmdloop()
        return 0

class BitbucketCmd(Cmd):
    """
    Bitbucket Cloud command interpreter.
    """

    def __init__(self):
        Cmd.__init__(self)

    def precmd(self, line):
        """
        Rewrite a command line to handle "EOF".
        """
        if line == 'EOF':
            line = 'exit'
            sys.stdout.write('exit\n')
        return line

    def do_exit(self, arg):
        """Terminate the shell."""
        # @todo Check the command arguments
        return True
