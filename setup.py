#!/usr/bin/env python
# setup.py - setup script for bitbucket-shell
# Copyright (C) 2017 Kaz Nishimura
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

from setuptools import setup

setup(
  name='bitbucket-shell',
  version='1.0',
  author='Kaz Nishimura',
  author_email='kazssym@vx68k.org',
  url='https://bitbucket.org/kazssym/bitbucket-shell',
  license='GPL-3.0+',
  description='Interactive command-line interface for Bitbucket Cloud',
  classifiers=[
    'Development Status :: 3 - Alpha',
    'Environment :: Console',
    'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    'Intended Audience :: Developers',
    'Topic :: Utilities',
    'Programming Language :: Python :: 2.7',
    'Programming Language :: Python :: 3',
  ],
  install_requires=[
    'requests',
  ],
  packages=[
    'bitbucketshell',
  ],
  entry_points={
    'console_scripts': [
      'bitsh = bitbucketshell.main:main',
    ],
  },
)
